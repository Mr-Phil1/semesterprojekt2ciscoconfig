package devices.interfaces;

import utils.Input;
import utils.Text;

import java.util.ArrayList;

public class InterfacesG extends Interfaces {

    public InterfacesG(String name) {
        super(name);
    }

    public void menu(int intNumber, ArrayList<String> tmp) {
        selection = false;
        while (!selection) {
            System.out.println(Interfaces.titelText(intNumber, false));
            System.out.print(interfaceMenuText());
            int input = Input.inputInt("Your Choice please: ", "[0-9]*");
            Text.printDash(100);
            switch (input) {
                case 1:
                    tmp.add(ipAddressSet());
                    tmp.add(interfaceEinAusschalten());
                    tmp.add("");
                    Text.printDash(100);
                    break;
                case 2:
                    tmp.add(addDescription());
                    Text.printDash(100);
                    break;
                case 3:
                    selection = true;
                    System.out.println("Good Bye!");
                    Text.printDash(100);
                    break;
                case 4:
                    selection = true;
                    setChange(true);
                    System.out.println("Good Bye8!");
                    Text.printDash(100);
                    break;
                default:
                    System.out.println(Text.errorTypingText("Numbers", "1-13"));
                    Text.printDash(100);
                    break;
            }
        }
    }


    @Override
    public String ipAddressSet() {
        return "ip address " + getIpAddress() + " " + getSubnetAddress();
    }
}
