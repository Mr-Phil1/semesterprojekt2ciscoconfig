package infra;

import devices.Device;
import devices.routers.routerModell.Router2911;
import utils.Input;
import utils.Text;

import java.io.IOException;

public class MenuRouter extends Menu {
    private String type;
    private Device d1;

    public MenuRouter(String type) {
        this.type = type;
    }

    private boolean selection = false;

    public String getType() {
        return type;
    }


    @Override
    public void menu() throws IOException {

    }

    public void selectModel() throws IOException {
        while (!selection) {
            System.out.println(Device.printMenuTitleText("Router"));
            System.out.print(selectModelText());
            int input = Input.intInput();
            Text.printDash(100);
            switch (input) {
                case 1:
                    selection = true;
                    System.out.println("Modell-829 " + notAvailable);
                    Text.printDash(100);
                    break;
                case 2:
                    selection = true;
                    System.out.println("Modell-1240 " + notAvailable);
                    Text.printDash(100);
                    break;
                case 3:
                    selection = true;
                    System.out.println("Modell-1841 " + notAvailable);
                    Text.printDash(100);
                    break;
                case 4:
                    selection = true;
                    System.out.println("Modell-1941 " + notAvailable);
                    Text.printDash(100);
                    break;
                case 5:
                    selection = true;
                    System.out.println("Modell-2602XM " + notAvailable);
                    Text.printDash(100);
                    break;
                case 6:
                    selection = true;
                    System.out.println("Modell-2621XM " + notAvailable);
                    Text.printDash(100);
                    break;
                case 7:
                    selection = true;
                    System.out.println("Modell-2811 " + notAvailable);
                    Text.printDash(100);
                    break;
                case 8:
                    selection = true;
                    System.out.println("Modell-2901 " + notAvailable);
                    Text.printDash(100);
                    break;
                case 9:
                    System.out.println("1");
                    d1 = new Router2911(getType(), "2911");
                    d1.menu();
                    break;
                case 10:
                    System.out.println("Modell-4321 " + notAvailable);
                    Text.printDash(100);
                    break;
                case 11:
                    System.out.println("Modell-4331 " + notAvailable);
                    Text.printDash(100);
                    break;
                case 12:
                    System.out.println("Modell-819HGW " + notAvailable);
                    Text.printDash(100);
                    break;
                case 13:
                    System.out.println("Modell-819IOX " + notAvailable);
                    Text.printDash(100);
                    break;
                case 14:
                    selection = true;
                    System.out.println("Good Bye");
                    Text.printDash(100);
                    break;
                default:
                    System.out.println(Text.errorTypingText("Numbers", "1-14"));
                    Text.printDash(100);
                    break;
            }
        }
    }

    @Override
    public String selectModelText() {
        return "Wohin möchten Sie: \n" +
                "\t01) Modell-829  \t02) Modell-1240 \t03) Modell-1841 \t04) Modell-1941 \n" +
                "\t05) Modell-2602XM \t06) Modell-2621XM  \t07) Modell-2811 \t08) Modell-2901 \n" +
                "\t09) Modell-2911 \t10) Modell-4321 \t11) Modell-4331 \t12) Modell-819HGW \n" +
                "\t13) Modell-819IOX \t14) Exit Router-Area\n" +
                "Ihre Eingabe bitte: ";
    }


}
