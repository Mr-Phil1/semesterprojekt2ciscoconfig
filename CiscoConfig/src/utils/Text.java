package utils;

public class Text {
    public static int titleDash = 100, tmpInt;

    public static String errorTypingText(String type, String range) {
        String tmpString = "";
        if (type != null && range != null) {
            tmpString = "\tYou have entered an incorrect value. Try it again. Only the following " + type + " are accepted: " + range + ".";
        }
        return tmpString;
    }

    public static void printDash(int length) {
        for (int i = 0; i < length; i++) {
            System.out.print("-");
        }
        System.out.println();
    }

    public static String printDashText(int length) {
        String tmpSring="\n";
        for (int i = 0; i < length; i++) {
            tmpSring += "-";
        }
        return tmpSring;
    }


    public static String printByeText() {
        return "Good Bye!";
    }

    // public  String menuText(){};
}
