package utils;

import infra.Selection;

import java.io.FileNotFoundException;

public class App {


    public void start() {
        Selection selection = new Selection();
        try {
            //selection.textLoop(21);
            selection.menu();
        } catch (FileNotFoundException e) {
            Debug.printDebug("File not found. " + e);
        } catch (IllegalArgumentException w) {
            Debug.printDebug("Illegal Argument");
            w.printStackTrace();
        } catch (java.io.IOException e) {
            Debug.printDebug(e);
        } catch (NullPointerException nullPointerException) {
            Debug.printDebug(nullPointerException);
        }
    }






}
