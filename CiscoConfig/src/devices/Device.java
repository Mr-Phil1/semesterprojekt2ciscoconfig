package devices;

import infra.IoUtils;
import utils.Input;
import utils.Text;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public abstract class Device {

    String hostname, deviceType, modelType, confTerminal = "conf t", printFileText = "Show your build config [y/n]: ", fileExt = ".txt", filePath = "src/output/config",
            inputStr, regexOnOff = "on|off|ON|OFF", theConfigFile = "The Config-File from the ", withHostname = " with the Hostname \"", tmpDevice, tmpString;
    public boolean selection = false, safeFile, password, change;
    public ArrayList<String> tmp;
    public HashMap<Integer, String> interfaceFList, interfaceGList;

    public Device() {
        this("UNDEFINED", "UNDEFINED");
    }

    public Device(String deviceType) {
        if (deviceType != null) {
            this.deviceType = stringToUpper(deviceType);
            interfaceFList = new HashMap<>();
            interfaceGList = new HashMap<>();
            interfaceFListFill();
        } else {
            throw new IllegalArgumentException();
        }
    }

    public Device(String deviceType, String modelType) {
        if (deviceType != null && modelType != null) {
            this.deviceType = stringToUpper(deviceType);
            this.modelType = modelType;
            interfaceFList = new HashMap<>();
            interfaceGList = new HashMap<>();
        } else {
            throw new IllegalArgumentException();
        }
    }

    public void setHostname(String hostname) {
        if (hostname != null && !hostname.equals(" ")) {
            this.hostname = hostname;
        } else {
            throw new IllegalArgumentException();
        }
    }

    public String getHostname() {
        return hostname;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void menu() throws IOException {
        tmp = new ArrayList<>();
        selection = false;
        //  printDash(titleDash);
        while (!selection) {
            System.out.print(printMenuTitleText(getDeviceType()));
            System.out.print(menuText());
            int input = Input.inputInt("Your Choice please: ", "[0-9]*");
            Text.printDash(Text.titleDash);
            switch (input) {
                case 1:
                    tmp = buildConfig(getDeviceType());
                    safeFile = true;
                    showBuildConfig();
                    Text.printDash(Text.titleDash);
                    selection = false;
                    break;
                case 2:
                    tmpDevice = getDeviceType();
                    inputStr = Input.checkStr("Enter Hostname [a-z,A-Z,-]: ", "[a-zA-Z0-9\\-]*");
                    if (new IoUtils().fileAvailable(filePath + tmpDevice + "/" + modelType + "-" + inputStr + fileExt)) {
                        System.out.println(theConfigFile + tmpDevice + modelType + "-" + withHostname + inputStr + "\" is:");
                        System.out.println("\t" + new IoUtils().readFile(new FileReader(filePath + tmpDevice + "/" + modelType + "-" + inputStr + fileExt)));
                    } else {
                        System.out.println(theConfigFile + getDeviceType() + modelType + "-" + withHostname + inputStr + "\" is not available.");
                    }
                    Text.printDash(Text.titleDash);
                    break;
                case 3:
                    if (safeFile) {
                        tmpDevice = getDeviceType();
                        new IoUtils().creatAFile(new File(filePath + tmpDevice + "/" + modelType + "-" + getHostname() + fileExt));
                        new IoUtils().writeFile(new FileWriter(filePath + tmpDevice + "/" + modelType + "-" + getHostname() + fileExt), tmp);
                        System.out.println("The Config-File is saved under: \n\t" + new IoUtils().fullFilePath(filePath + tmpDevice + "/" + modelType + "-" + getHostname() + fileExt));
                        //tmp.clear();
                        System.out.println(Text.printByeText());
                    } else {
                        System.out.println("First you must write a Config!");
                    }
                    Text.printDash(Text.titleDash);
                    break;
                case 4:
                    selection = true;
                    System.out.println(stringToUpper(getDeviceType()));
                    System.out.println(Text.printByeText());
                    Text.printDash(Text.titleDash);
                    break;
                default:
                    System.out.println(Text.errorTypingText("Numbers", "1-4"));
                    Text.printDash(Text.titleDash);
                    break;
            }
        }
    }

    public ArrayList<String> buildConfig(String deviceType) {
        tmp = new ArrayList<>();
        System.out.println("THE CONFIG-BUILDER!");
        setHostname(Input.checkStr("Set Hostname [a-z,A-Z,0-9,-]: ", "[a-zA-Z]+[a-zA-Z0-9\\-]*"));
        tmp.add("!This File is created by");
        tmp.add("!the Config-Builder by Mr. Phil");
        tmp.add("!This is the Config-File for the " + deviceType + " " + modelType + ": " + getHostname());
        tmp.add("en");
        tmp.add(confTerminal);
        tmp.add("hostname " + getHostname());
        addNotEmpty(domainLookupText());
        addEmptyLine();
        tmp.add(setMotdBanner());
        addNotEmpty(setSecretText());
        addNotEmpty(passwordEncText());
        tmp.add("!-------------------------");
        return tmp;
    }

    public boolean domainLookupBool() {
        inputStr = Input.checkStr("Command Check (domain look up) [on/off]: ", regexOnOff).toLowerCase();
        if (inputStr.equals("off")) {
            return true;
        } else {
            return false;
        }
    }

    public String domainLookupText() {
        if (domainLookupBool()) {
            return "no ip domain lookup";
        }
        return "";
    }

    public boolean setSecretBool() {
        inputStr = Input.checkStr("Enable Password [on/off]: ", regexOnOff).toLowerCase();
        if (inputStr.equals("on")) {
            password = true;
            return true;
        } else {
            password = false;
            return false;
        }
    }

    public String setSecretText() {
        if (setSecretBool()) {
            System.out.print("What should the Enable-Password be: ");
            inputStr = Input.inputString();
            return "enable secret " + inputStr;
        } else {
            return "";
        }
    }

    public boolean passwordEncBool() {
        inputStr = Input.checkStr("Password encryption [on/off]: ", regexOnOff).toLowerCase();
        if (inputStr.equals("on")) {
            return true;
        } else {
            return false;
        }
    }

    public String passwordEncText() {
        inputStr = "";
        if (password) {
            if (passwordEncBool()) {
                inputStr = "service password-encryption";
            }
        } else {
            inputStr = "";
        }
        System.out.println(inputStr);
        return inputStr;
    }

    public String setMotdBanner() {
        inputStr = Input.checkStr("What should the MOTD be: ", "[a-zA-Z\\-!]*");
        return "banner motd # " + inputStr + " #";
    }

    public static String printMenuTitleText(String device) {
        String tmpString;
        if (device != null) {
            if (device.equalsIgnoreCase("router")) {
                tmpString = "+-+-+-+-+-+-+\n" +
                        "|R|o|u|t|e|r|\n" +
                        "+-+-+-+-+-+-+\n";
            } else {
                tmpString = "+-+-+-+-+-+-+\n" +
                        "|S|w|i|t|c|h|\n" +
                        "+-+-+-+-+-+-+\n";
            }
        } else {
            throw new IllegalArgumentException();
        }
        return tmpString;
    }

    public void abschnittWechseln(boolean end) {
        if (end) {
            tmp.add("end");
        } else {
            tmp.add("exit");
        }
    }

    public void addEmptyLine() {
        tmp.add("");
    }

    public String stringToUpper(String input) {
        if (input != null && !Character.isUpperCase(input.charAt(0)) || Character.isUpperCase(input.charAt(0))) {
            tmpString = input.substring(0, 1).toUpperCase() + input.substring(1, input.length());
        } else {
            throw new IllegalArgumentException();
        }
        return tmpString;
    }

    public void addNotEmpty(String input) {
        if (input != null) {
            tmp.add(input);
        } else {
            throw new IllegalArgumentException();
        }
    }

    public void showBuildConfig() {
        Text.printDash(30);
        String check = Input.checkStr(printFileText, "y|n|Y|N").toLowerCase();
        if (check.equals("y")) {
            System.out.println("Your build Config is: \n\t");
            for (String i : tmp) {
                System.out.println("\t" + i);
            }
        }
    }

    public String menuText() {
        return "How can I Help You? \n" +
                "\t1) Make New Config \t2) Show old config \t3) Save config to File \t4) Exit Section \n";
    }

    public void interfaceMenu(){
        System.out.print(interfaceText());
    };

    public abstract void interfaceFListFill();

    public abstract void interfaceGListFill();

    public String interfaceText() {
        return "List of all avaibile Interfaces:"+Text.printDashText(35);
    }

    @Override
    public String toString() {
        return "Device{" +
                "hostname='" + hostname + '\'' +
                ", modelNumber='" + modelType + '\'' +
                '}';
    }

}
