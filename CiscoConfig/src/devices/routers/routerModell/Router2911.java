
package devices.routers.routerModell;

import devices.interfaces.InterfacesF;
import devices.routers.Router;
import utils.Input;
import utils.Text;

import java.util.ArrayList;

public class Router2911 extends Router {
    public InterfacesF interfacesF;

    public Router2911(String deviceType, String model) {
        super(deviceType, model);
    }

    public ArrayList<String> buildConfig(String deviceType) {
        tmp = super.buildConfig(deviceType);
        System.out.println(interfaceFList.get(7));
        while (!change) {
            interfaceMenu();
            change = interfacesF.isChange();
        }
        return tmp;
    }

    @Override
    public void interfaceMenu() {
        super.interfaceMenu();
        Text.printDash(35);
        int input = Input.inputInt("Your Choice please: ", "[1-9]|[12][0-4]");
        interfacesF = new InterfacesF(interfaceFList.get(input));
        tmp.add(interfacesF.setInterface());
        Text.printDash(100);
        interfacesF.menu(input, tmp);
    }


    @Override
    public void interfaceFListFill() {

    }

    @Override
    public void interfaceGListFill() {

    }

}
