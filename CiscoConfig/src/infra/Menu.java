package infra;

import java.io.IOException;

public abstract class Menu {

    private boolean selection = false;
    public String notAvailable="is not available!";

    public abstract void menu() throws IOException;

    public abstract void selectModel() throws IOException;



  /*  public String menuText() {
        return "Wohin möchten Sie: \n" +
                "\t1) Router \t2) Switch \t3)  Programm beenden \n" +
                "Ihre Eingabe bitte: ";
    }*/


    public abstract String selectModelText();


}
