package devices.interfaces;

import utils.Input;
import utils.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public abstract class Interfaces {
    private String name, tmpString, regexIp = "[01]?\\d{1,2}|2[0-4]\\d|25[0-5]";
    private final HashMap<Integer, String> SUBNET_MAP;
    public boolean selection, change;

    public Interfaces(String name) {
        this.name = name;
        SUBNET_MAP = new HashMap<>();
        fillSubnetMap();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChange() {
        return change;
    }

    public void setChange(boolean change) {
        this.change = change;
    }

    public abstract void menu(int intNumber, ArrayList<String> tmp);

    public String interfaceMenuText() {
        return "How can I Help You?" +
                "\n\t01) IP-Address set " +
                "\t02) Add Interface Description " +
                "\t03) Change Interface" +
                "\t04) Exit Interface Edit\n";
    }

    public String showInterface() {
        return "show interface " + getName();
    }

    public String setInterface() {
        return "int " + getName();
    }

    public boolean interfaceEinAusschaltenBool() {
        String inputStr = Input.checkStr("Interface [on/off]: ", "on|off|ON|OFF").toLowerCase();
        if (inputStr.equals("on")) {
            return true;
        } else {
            return false;
        }
    }

    public String interfaceEinAusschalten() {
        if (interfaceEinAusschaltenBool()) {
            return "no shut";
        } else {
            return "shut";
        }
    }

    public abstract String ipAddressSet();

    public String getSubnetAddress() {
        this.alleSubnetMaskenAusgeben();
        int input = Input.inputInt("Enter the Subnet-ID please: ", "[8-9]|[12][0-9]|3[0]");
        return SUBNET_MAP.get(input);
    }

    public String getIpAddress() {
        int input1 = Input.inputInt("Your Choice for the first segment of the IP-Address please: ", regexIp);
        int input2 = Input.inputInt("Your Choice for the second segment of the IP-Address please: ", regexIp);
        int input3 = Input.inputInt("Your Choice for the third segment of the IP-Address please: ", regexIp);
        int input4 = Input.inputInt("Your Choice for the fourth segment of the IP-Address please: ", regexIp);
        tmpString = input1 + "." + input2 + "." + input3 + "." + input4;
        System.out.println("You're IP-Address is " + tmpString);
        Text.printDash(65);
        return tmpString;
    }

    private void fillSubnetMap() {
        SUBNET_MAP.put(8, "255.0.0.0");
        SUBNET_MAP.put(9, "255.128.0.0");
        SUBNET_MAP.put(10, "255.192.0.0");
        SUBNET_MAP.put(11, "255.224.0.0");
        SUBNET_MAP.put(12, "255.240.0.0");
        SUBNET_MAP.put(13, "255.248.0.0");
        SUBNET_MAP.put(14, "255.252.0.0");
        SUBNET_MAP.put(15, "255.254.0.0");
        SUBNET_MAP.put(16, "255.255.0.0");
        SUBNET_MAP.put(17, "255.255.128.0");
        SUBNET_MAP.put(18, "255.255.192.0");
        SUBNET_MAP.put(19, "255.255.224.0");
        SUBNET_MAP.put(20, "255.255.240.0");
        SUBNET_MAP.put(21, "255.255.248.0");
        SUBNET_MAP.put(22, "255.255.252.0");
        SUBNET_MAP.put(23, "255.255.254.0");
        SUBNET_MAP.put(24, "255.255.255.0");
        SUBNET_MAP.put(25, "255.255.255.128");
        SUBNET_MAP.put(26, "255.255.255.192");
        SUBNET_MAP.put(27, "255.255.255.224");
        SUBNET_MAP.put(28, "255.255.255.240");
        SUBNET_MAP.put(29, "255.255.255.248");
        SUBNET_MAP.put(30, "255.255.255.252");
    }

    private boolean addDescriptionBool() {
        String inputStr = Input.checkStr("Add Description [yes/no]: ", "yes|no|YES|NO").toLowerCase();
        if (inputStr.equals("yes")) {
            return true;
        } else {
            return false;
        }
    }

    public String addDescription() {
        if (addDescriptionBool()) {
          tmpString=  Input.checkStr("Set Description [a-z,A-Z,0-9,-]: ", "[a-zA-Z]+[a-zA-Z0-9\\-]*");
            return "description "+tmpString;
        } else {
            return "";
        }
    }

    public void alleSubnetMaskenAusgeben() {
        int num = 0;
        System.out.println("ID : Subnet-Mask");
        Text.printDash(16);
        for (Map.Entry<Integer, String> pair : SUBNET_MAP.entrySet()) {
            if (pair.getKey() < 10) {
                System.out.print("\t0" + pair.getKey() + " : " + pair.getValue() + "  ");
            } else {
                System.out.print("\t" + pair.getKey() + " : " + pair.getValue() + "  ");
            }
            num++;
            if (num % 5 == 0) {
                System.out.println();
            }
        }
        System.out.println();
    }

   /* public void alleSubnetMaskenAusgeben() {
        System.out.println("Subnet-Mask:\n\tID: Subnet-Mask");
        this.SUBNET_MAP.forEach((key, value) -> {
            System.out.println("\t" + key + " : " + value);
        });
    }*/

    public static String titelText(int number, boolean fast) {
        String y, x, tmp;
        if (number < 10) {
            y = "0";
            x = String.valueOf(number).substring(0);
        } else {
            y = String.valueOf(number).substring(0, 1);
            x = String.valueOf(number).substring(1);
        }
        if (fast) {
            tmp = " +-+-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+\n" +
                    " |I|n|t|e|r|f|a|c|e| |F|a|s|t|-|" + y + "|" + x + "|\n" +
                    " +-+-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+\n";
        } else {
            tmp = " +-+-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+\n" +
                    " |I|n|t|e|r|f|a|c|e| |G|i|g|a|-|" + y + "|" + x + "|\n" +
                    " +-+-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+\n";
        }
        return tmp;
    }

    public void textLoop(int loop) {
        for (int i = 0; i < loop; i++) {
            System.out.println(titelText(i, true));
            System.out.println(titelText(i, false));
        }

    }

}