package infra;

import fileio.FileRead;
import fileio.FileWrite;

import java.io.*;
import java.util.ArrayList;

public class IoUtils implements FileRead, FileWrite {
    private ArrayList<String> ausgabe = new ArrayList<>();

    @Override
    public void readFile1(FileReader fileName) throws IOException {

    }

    @Override
    public boolean fileAvailable(String fileName) throws IOException {
        return new File(fileName).exists();
    }

    /**
     * @param fileName
     * @return
     * @throws IOException
     */
    @Override
    public ArrayList<String> readFile(FileReader fileName) throws IOException, NullPointerException {
        String tmpString;
        BufferedReader bufferedReader = new BufferedReader(fileName);
        ArrayList<String> output = new ArrayList<>();
        while ((tmpString = bufferedReader.readLine()) != null) {
            output.add(tmpString);

        }
        return output;
    }

    /**
     * @param filename h
     * @param content  h
     * @throws IOException g
     */
    @Override
    public void writeFile(FileWriter filename, ArrayList<String> content) throws IOException, NullPointerException {
        BufferedWriter bufferedWriter = new BufferedWriter(filename);
        if (content.size() != 0) {
            for (String line : content) {
                bufferedWriter.write(line);
                bufferedWriter.newLine();
            }
            bufferedWriter.close();
        } else {
            System.out.println("can not write file");
        }

    }

    @Override
    public String fullFilePath(String fileName) throws IOException, NullPointerException {
        File tmp = new File(fileName);
        return tmp.getAbsolutePath();
    }

    @Override
    public String filePath(String fileName) throws IOException, NullPointerException {
        File tmp = new File(fileName);
        return tmp.getPath();
    }


    /**
     * @param filename
     * @throws IOException
     */
    @Override
    public void writeFile(FileWriter filename) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(filename);
        bufferedWriter.write("lo8rem lipsum\nltesh i");
        bufferedWriter.close();
    }

    @Override
    public String ausgabe() {
        return "nusll";
    }

    @Override
    public void creatAFile(File fileName) throws IOException {
        fileName.createNewFile();
    }


}
