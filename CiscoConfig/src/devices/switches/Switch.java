package devices.switches;

import devices.Device;

import java.util.ArrayList;

public abstract class Switch extends Device {
    public Switch(String deviceType) {
        super(deviceType);
    }

    public Switch(String hostname, String modelNumber) {
        super(hostname, modelNumber);
    }

    @Override
    public ArrayList<String> buildConfig(String deviceType) {
        tmp = super.buildConfig(deviceType);
        tmp.add("!Switch-Config");
        return tmp;
    }

    @Override
    public String toString() {
        return "Switch{} " + super.toString();
    }
}


