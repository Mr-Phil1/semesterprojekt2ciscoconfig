---
title: Semester-Projekt 2 
subtitle: Cisco-Builder
author: Mr. Phil
rights: 
language: de-AT
keywords: [JAVA, POS-P, NVS, Cisco]
titlepage: true
titlepage-color: "FF6D42"
table-use-row-colors: true
toc-own-page: true
header-right: "Beschreibung von Version 0.0.1 Pre-Alpha"
---

# Implementation eines Cisco Config Programmes

- [x] **Is available**
- [ ] **Is not available**

## Feature: 
### Existing Feature:
- [x] While-Loop Interface selecton
- [x] Set Legacy-IP Address
- [x] Add Modellnumber into the filename
- [x] Add Modellnumber into the config-file
- [x] Interface-Description
- [x] Interface on/off status choice


### Futher-Feature:
- [ ]  GUI
- [ ]  VLAN
- [ ]  Set IPv6-Adress
- [ ]  All the other Switch or Router Modells


## Credits:
### Creator:

  * Mr. Phil


### Technical consultant:

  * Moser Dominik
  * Landerer Claudio
  
### Beta-Tester:

  * Kutz Thomas
  
\pagebreak
## Supported Devices:
### Switch:

- [ ] Modell-2950T
- [x] **Modell-2950_24**
- [ ] Modell-2960
- [ ] Modell-3560_24PS
- [ ] Modell-3650_24PS


### Router: 

- [ ] Modell-829 
- [ ] Modell-1240 
- [ ] Modell-1841
- [ ] Modell-1941
- [ ] Modell-2602XM
- [ ] Modell-2621XM
- [ ] Modell-2811
- [ ] Modell-2901
- [x] **Modell-2911**
- [ ] Modell-4321
- [ ] Modell-4331
- [ ] Modell-819HGW
- [ ] Modell-819IOX

