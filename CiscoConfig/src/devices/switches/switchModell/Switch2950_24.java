package devices.switches.switchModell;

import devices.interfaces.Interfaces;
import devices.interfaces.InterfacesF;
import devices.switches.Switch;
import utils.Input;
import utils.Text;

import java.io.IOException;
import java.util.ArrayList;

public class Switch2950_24 extends Switch {
    public Interfaces interfaces;

    public Switch2950_24(String deviceType, String model) {
        super(deviceType, model);
        interfaceFListFill();
    }

    @Override
    public void menu() throws IOException {

      super.menu();
    }

    @Override
    public ArrayList<String> buildConfig(String deviceType) {
        tmp = super.buildConfig(deviceType);
        while (!change) {
            interfaceMenu();
            change = interfaces.isChange();
        }
        return tmp;
    }

    @Override
    public void interfaceMenu() {
        super.interfaceMenu();
        int input = Input.inputInt("Your Choice please: ", "[0-9]|1[0-9]|2[0-4]");
        interfaces = new InterfacesF(interfaceFList.get(input));
        tmp.add("");
        tmp.add(interfaces.setInterface());
        Text.printDash(100);
        interfaces.menu(input, tmp);

    }

    @Override
    public String interfaceText() {
        return super.interfaceText() +
                "\n\t01) FastEthernet0/1 " +
                "\t02) FastEthernet0/2 " +
                "\t03) FastEthernet0/3 " +
                "\n\t04) FastEthernet0/4 " +
                "\t05) FastEthernet0/5 " +
                "\t06) FastEthernet0/6 " +
                "\n\t07) FastEthernet0/7 " +
                "\t08) FastEthernet0/8 " +
                "\t09) FastEthernet0/9 " +
                "\n\t10) FastEthernet0/10" +
                "\t11) FastEthernet0/11" +
                "\t12) FastEthernet0/12" +
                "\n\t13) FastEthernet0/13" +
                "\t14) FastEthernet0/14" +
                "\t15) FastEthernet0/15" +
                "\n\t16) FastEthernet0/16" +
                "\t17) FastEthernet0/17" +
                "\t18) FastEthernet0/18" +
                "\n\t19) FastEthernet0/19" +
                "\t20) FastEthernet0/20" +
                "\t21) FastEthernet0/21" +
                "\n\t22) FastEthernet0/22" +
                "\t23) FastEthernet0/23" +
                "\t24) FastEthernet0/24" +
                "\n";
    }

    @Override
    public void interfaceFListFill() {
        interfaceFList.put(0, "FastEthernet0/0");
        interfaceFList.put(1, "FastEthernet0/1");
        interfaceFList.put(2, "FastEthernet0/2");
        interfaceFList.put(3, "FastEthernet0/3");
        interfaceFList.put(4, "FastEthernet0/4");
        interfaceFList.put(5, "FastEthernet0/5");
        interfaceFList.put(6, "FastEthernet0/6");
        interfaceFList.put(7, "FastEthernet0/7");
        interfaceFList.put(8, "FastEthernet0/8");
        interfaceFList.put(9, "FastEthernet0/9");
        interfaceFList.put(10, "FastEthernet0/10");
        interfaceFList.put(11, "FastEthernet0/11");
        interfaceFList.put(12, "FastEthernet0/12");
        interfaceFList.put(13, "FastEthernet0/13");
        interfaceFList.put(14, "FastEthernet0/14");
        interfaceFList.put(15, "FastEthernet0/15");
        interfaceFList.put(16, "FastEthernet0/16");
        interfaceFList.put(17, "FastEthernet0/17");
        interfaceFList.put(18, "FastEthernet0/18");
        interfaceFList.put(19, "FastEthernet0/19");
        interfaceFList.put(20, "FastEthernet0/20");
        interfaceFList.put(21, "FastEthernet0/21");
        interfaceFList.put(22, "FastEthernet0/22");
        interfaceFList.put(23, "FastEthernet0/23");
        interfaceFList.put(24, "FastEthernet0/24");

    }

    @Override
    public void interfaceGListFill() {

    }


}
