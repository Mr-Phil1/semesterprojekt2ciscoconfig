package infra;

import devices.Device;
import devices.switches.switchModell.Switch2950_24;
import utils.Input;
import utils.Text;

import java.io.IOException;

public class MenuSwitch extends Menu {
    private String type;
    private boolean selection = false;
    private Device d1;

    public MenuSwitch(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

  /*  public void menu() throws IOException {
        while (!selection) {
            System.out.print(menuText());
            int input = Input.intInput();
            switch (input) {
                case 1:
                    selectModel();
                    break;
                case 2:
                    selection = true;
                    break;
                default:
                    System.out.println(errorTypingText("Numbers", "1-2"));
                    printDash(100);
                    break;
            }
        }
    }*/

    @Override
    public void menu() throws IOException {

    }

    @Override
    public void selectModel() throws IOException {
        while (!selection) {
            System.out.print(selectModelText());
            int input = Input.intInput();
            Text.printDash(100);
            switch (input) {
                case 1:
                    d1 = new Switch2950_24(getType(), "2950_24");
                    d1.menu();
                    break;
                case 2:
                    System.out.println("Modell-2950T "+notAvailable);
                    break;
                case 3:
                    System.out.println("Modell-2960 "+notAvailable);
                    break;
                case 4:
                    System.out.println("Modell-3560_24PS "+notAvailable);
                    break;
                case 5:
                    System.out.println("Modell-3650_24PS "+notAvailable);
                    break;
                case 6:
                    selection = true;
                    System.out.println("Good Bye!");
                    Text.printDash(100);
                    break;
                default:
                    System.out.println(Text.errorTypingText("Numbers", "1-5"));
                    Text.printDash(100);
                    break;
            }
        }
    }

    @Override
    public String selectModelText() {
        return "Wohin möchten Sie: \n" +
                "\t1) Model-2950_24 \t\t2) Modell-2950T \t\t3) Modell-2960\n" +
                " \t4) Modell-3560_24PS \t5) Modell-3650_24PS \t6) Exit Switch-Area \n" +
                "Ihre Eingabe bitte: ";
    }

}
