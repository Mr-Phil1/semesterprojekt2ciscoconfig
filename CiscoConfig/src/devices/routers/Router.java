package devices.routers;

import devices.Device;

import java.util.ArrayList;

public abstract class Router extends Device {
    public Router(String deviceType) {
        super(deviceType);
    }

    public Router(String hostname, String modelNumber) {
        super(hostname, modelNumber);
    }

    @Override
    public ArrayList<String> buildConfig(String deviceType) {
        tmp = super.buildConfig(deviceType);
        tmp.add("!Router-Config");
        showBuildConfig();
        return tmp;
    }





    @Override
    public String toString() {
        return "Router{} " + super.toString();
    }
}
