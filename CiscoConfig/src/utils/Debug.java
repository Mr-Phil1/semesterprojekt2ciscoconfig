package utils;

public class Debug {
    /**
     *
     */
    private static boolean print = true;

    /**
     * Wenn in die interne print-Variable auf true steht wird die mitgegebene Message ausgegeben
     * @param message Hier kann die gewünschte Message mitgegeben werden.
     */
    public static void printDebug(Object message){
        if (print) {
            System.out.println(message);
        }
    }
}
