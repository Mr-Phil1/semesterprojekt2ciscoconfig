package infra;

import utils.Input;
import utils.Text;

import java.io.IOException;

public class Selection extends Menu {
    private boolean selection;
    private Menu menu;

    public Selection() {
        selection = false;
    }


    public void menu() throws IOException {
        selection = false;
        Text.printDash(100);
        while (!selection) {
            System.out.print(menuText());
            int input = Input.inputInt("Your Choice please: ", "[0-9]*");
            Text.printDash(100);
            switch (input) {
                case 1:
                    menu = new MenuRouter("Router");
                    menu.selectModel();
                    break;
                case 2:
                    menu = new MenuSwitch("Switch");
                    menu.selectModel();
                    break;
                case 3:
                    info();
                    selection = false;
                    break;
                case 4:
                    selection = true;
                    System.out.println("Good Bye!");
                    Text.printDash(100);
                    break;
                default:
                    System.out.println(Text.errorTypingText("Numbers", "1-4"));
                    Text.printDash(100);
                    break;
            }
        }
    }

    @Override
    public void selectModel() throws IOException {

    }

    public String menuText() {
        return "How can I Help You? \n" +
                "\t1) Router-Config \t2) Switch-Config \t3) Show Info \t4) Terminate Program\n";
    }

    @Override
    public String selectModelText() {
        return null;
    }

    public void info() {
        selection = false;
        while (!selection) {
            System.out.println(infoTitle());
            System.out.print(menuTextInfo());
            int input = Input.inputInt("Your Choice please: ", "[0-9]*");
            Text.printDash(100);
            switch (input) {
                case 1:
                    System.out.println(supportedRouter());
                    Text.printDash(100);
                    break;
                case 2:
                    System.out.println(supportedSwitch());
                    Text.printDash(100);
                    break;
                case 3:
                    System.out.println(creditTitle());
                    System.out.println(showCoWorkersText());
                    Text.printDash(100);
                    break;
                case 4:
                    selection = true;
                    System.out.println("Good Bye!");
                    Text.printDash(100);
                    break;
                default:
                    System.out.println(Text.errorTypingText("Numbers", "1-4"));
                    Text.printDash(100);
                    break;
            }
        }
    }

    private String menuTextInfo() {
        return "How can I Help You? \n" +
                "\t1) Show all available Router \t2) Show all available Switch \t3) Show Credits \t4) Terminate Program\n";
    }

    private String supportedRouter() {
        return "List of all the aupported Router:" +
                "\n\t01) Modell-829  " +
                "\t02) Modell-1240 " +
                "\t03) Modell-1841 " +
                "\n\t04) Modell-1941 " +
                "\t05) Modell-2602XM " +
                "\t06) Modell-2621XM " +
                "\n\t07) Modell-2811 " +
                "\t08) Modell-2901 " +
                "\t09) Modell-2911 " +
                "\n\t10) Modell-4321 " +
                "\t11) Modell-4331 " +
                "\t12) Modell-819HGW " +
                "\t13) Modell-819IOX";
    }

    private String supportedSwitch() {
        return "List of all the available Switch:" +
                "\n\t01) Modell-2950T" +
                "\t\t02) Modell-2950_24 " +
                "\t03) Modell-2960 " +
                "\n\t04) Modell-3560_24PS " +
                "\t05) Modell-3650_24PS";
    }

    private String infoTitle() {
        return " +-+-+-+-+\n" +
                " |I|n|f|o|\n" +
                " +-+-+-+-+\n";
    }

    private String creditTitle() {
        return " +-+-+-+-+-+-+-+\n" +
                " |C|r|e|d|i|t|s|\n" +
                " +-+-+-+-+-+-+-+\n";
    }

    private String showCoWorkersText() {
        return "Creator: " +
                "\n\t*) Mr. Phil" +
                "\nTechnical consultant: " +
                "\n\t*) Moser Dominik \n\t*) Landerer Claudio" +
                "\nBeta-Tester: " +
                "\n\t*) Kutz Thomas";
    }
}
