package utils;

import java.util.Scanner;

public class Input {
    private static Scanner scan = new Scanner(System.in);

    /**
     * @param message
     * @param regex
     * @return Liefert den eingegeben Wert als int zurück.
     */
    public static int inputInt(String message, String regex) {
        return Input.checkInt(message, regex);
    }

    public static int intInput() {
        return scan.nextInt();
    }

    /**
     * @param message
     * @param regex
     * @return Liefert den eingegeben Wert als double zurück.
     */
    public static double inputDouble(String message, String regex) {
        return Double.parseDouble(Input.checkerStr(message, regex));
    }

    /**
     * @return Liefert den eingegeben Wert als String zurück.
     */
    public static String inputString() {
        return scan.next();
    }

    /**
     * @return Liefert den eingegeben Wert als char zurück.
     */
    public static char inputChar() {
        return scan.next().charAt(0);
    }
/*
    private static int checkInt(String message, String regex) {
        int tmp = 0;
        boolean falseEntry = true;
        do {
            try {
                System.out.print(message);
                tmp = Integer.parseInt((Input.checkerStr(message, regex)));
            } catch (NumberFormatException n) {
                System.out.print(message);
                falseEntry = true;
            }
        } while (falseEntry);
        return tmp;
    }*/
private static int checkInt(String message, String regex) {
    return Integer.parseInt((Input.checkerStr(message, regex)));
}


    private static String checkerStr(String message, String regex) {
        String checkStr;
        do {
            System.out.print(message);
            checkStr = Input.inputString();
        } while (!(checkStr.matches(regex)));
        return checkStr;
    }

    public static String checkStr(String message, String regex) {
        return Input.checkerStr(message, regex);
    }

    public static double checkDouble(String message, String regex) {
        return Double.parseDouble(Input.checkerStr(message, regex));
    }
}
