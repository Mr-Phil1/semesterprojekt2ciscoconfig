package fileio;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public interface FileRead {
    void readFile1(FileReader fileName) throws IOException;

    boolean fileAvailable(String fileName) throws IOException;

    ArrayList<String> readFile(FileReader fileName) throws IOException;
}
