package fileio;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public interface FileWrite {
    void writeFile(FileWriter filename) throws IOException;

    void writeFile(FileWriter filename, ArrayList<String> content) throws IOException, NullPointerException;

    String fullFilePath(String fileName) throws IOException, NullPointerException;

    String filePath(String fileName) throws IOException, NullPointerException;

    String ausgabe();

    void creatAFile(File fileName) throws IOException;
}
